#!/bin/bash

#====================================================#
# this shell script simply creates project structure #
# for a html/css/js/php website, while adding the    #
# database files required to access database (MySql) #
# via php. the script assumes apache as the webserver#
# and linux as the OS platform on which it runs.     #
#====================================================#


# creating required functions

# unroll: this function should be called at any moment the script fails and is required to be 
# restarted except for root previlages restart. the function will simply delete the folder with 
# name $2 in the destination /var/www/html.
# function arguments: $1 -> project name 			
#					  
function unroll {
	if [ -d /var/www/html/$project_name ]
		then
		rm -r /var/www/html/$project_name
	fi
}

# createDB: this function should be called to create required dbs for the framework.
# notes:     mysql will promote user to enter his root password.
# function arugments: 	$1 -> project_name
# 						$2 -> user_name
#						$3 -> user_pass
function createDB {
	echo "invoking mysql with root previlage."

	mysql -u root -p -e 'CREATE DATABASE IF NOT EXISTS '"$project_name"';
	GRANT ALL PRIVILEGES ON '"$project_name"'.* TO '"'$user_name'"'@'"'localhost'"' IDENTIFIED BY '"'$user_pass'"';
	FLUSH PRIVILEGES;
	USE '"$project_name"';
	
	CREATE TABLE  IF NOT EXISTS `pages` (
	 `pid` int(100) NOT NULL,
	 `page_content` longtext COLLATE utf8_bin NOT NULL,
	 `clearance_level` int(1) NOT NULL,
	 `attr` int(11) DEFAULT NULL,
	 `createdAt` DATETIME 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

	ALTER TABLE `pages` ADD PRIMARY KEY (`pid`);
	ALTER TABLE `pages` MODIFY `pid` int(100) NOT NULL AUTO_INCREMENT;

	CREATE TABLE IF NOT EXISTS `options` (`opid` int(100) NOT NULL, `op_name` varchar(60)  NOT NULL, `op_value` varchar(100) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
	
	ALTER TABLE `options` ADD PRIMARY KEY (`opid`);
	ALTER TABLE `options` MODIFY `opid` int(100) NOT NULL AUTO_INCREMENT;

	CREATE TABLE  IF NOT EXISTS `account` (`aid` int(100) NOT NULL, `first_name` varchar(100)  NOT NULL, `middle_name` varchar(100) NOT NULL, `last_name` varchar(100) NOT NULL, 
	`email` varchar(100) NOT NULL, `username` varchar(100) NOT NULL, `password` varchar(100) NOT NULL, 
	`b_date` date NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

	ALTER TABLE `account` ADD PRIMARY KEY (`aid`);
	ALTER TABLE `account` MODIFY `aid` int(100) NOT NULL AUTO_INCREMENT;

	CREATE TABLE  IF NOT EXISTS `admin` (`adid` int(100) NOT NULL, `username` varchar(100)  NOT NULL, `email` varchar(100) NOT NULL, `password` varchar(100) NOT NULL, 
	`clearance_level` int(1) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
	
	ALTER TABLE `admin` ADD PRIMARY KEY (`adid`);
	ALTER TABLE `admin` MODIFY `adid` int(100) NOT NULL AUTO_INCREMENT;

	CREATE TABLE  IF NOT EXISTS `admin_options` (`adopid` int(100) NOT NULL, `feature` varchar(100)  NOT NULL, `admin_id` int(100) NOT NULL, INDEX adid (admin_id), FOREIGN KEY (admin_id) REFERENCES admin(adid) ON DELETE CASCADE ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
	
	ALTER TABLE `admin_options` ADD PRIMARY KEY (`adopid`);
	ALTER TABLE `admin_options` MODIFY `adopid` int(100) NOT NULL AUTO_INCREMENT;


	CREATE TABLE  IF NOT EXISTS `features` (`fid` int(100) NOT NULL, `title` varchar(100)  NOT NULL, `ips_no` int(100) NOT NULL, `ops_no` int(100) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
	
	ALTER TABLE `features` ADD PRIMARY KEY (`fid`);
	ALTER TABLE `features` MODIFY `fid` int(100) NOT NULL AUTO_INCREMENT;



	INSERT INTO `options` (`op_name` , `op_value`) VALUES ('"'comments', 'f0'"');
	INSERT INTO `options` (`op_name` , `op_value`) VALUES ('"'categories', 'f0'"');
	INSERT INTO `options` (`op_name` , `op_value`) VALUES ('"'tags', 'f0'"');'

	if [ $? -eq 0 ]; 
	then
		echo "MySQL database and user created."
		echo "Database Name: $project_name"
		echo "Username     : $user_name"
		echo "password     : $user_pass"
		
		echo "creating php config file for your db."
		CNFPATH=/var/www/html/$project_name/includes/
		if [ ! -f $CNFPATH/config.php ]
			then
			touch $CNFPATH/config.php
			touch $CNFPATH/'.bu'
		fi

		cat << EOF > $CNFPATH/config.php
<?php
  // 1. Create a database connection
  define("DB_SERVER", 'localhost');
  define("DB_USER", '$user_name');
  define("DB_PASS", '$user_pass');
  define("DB_NAME", '$project_name');
?>
EOF

		cat << EOF > $CNFPATH/'.bu.txt'
		${user_name}
		${user_pass}
		${project_name}
EOF

		return 0
	else
		echo "failed to create new db user."
		return 1
	fi
	

}

SELF=$0
echo "Creating Project Herierchy Script Started"

#previlage checks

if [ "$(whoami)" != "root" ];
then
		echo "need to be root to execute this shell script. re-executing..."
		exec sudo -- "$0" "$@"			

fi
#==============================================================================#

# required dependencies check

# checking for apache
command -v apache2 >/dev/null 2>&1 || { echo >&2 "the FW requires apache2 to exist. please install then re-run."; exit 1; }

# checking for mysql db
command -v mysql >/dev/null 2>&1 || { echo >&2 "the FW requires mysql to exist. please install then re-run."; exit 1; }

# required directories 
dir[0]="classes"
dir[1]="includes"
dir[2]="js"
dir[3]="css"
dir[4]="/var/www/html"
dir[5]="fonts"
dir[6]="admin"

# required files
files[0]="index.php"
files[1]="headerDesign.php"
files[2]="footerDesign.php"
files[3]="sync.sh"

# checking existance of files and directories
# TODO: create script called exists, that checks if the files structure is correct within directories. if the script
# returned a 1 then there is an error and we should roll back and exit. otherwise we continue execution normally.
for i in `seq 0 6`; do
	if [ ! -d ${dir[$i]} ]; then
		echo "${dir[$i]} directory does not exist. please ensure it exists in the same path as this script and try again."
		exit $i
	fi
done 

# checking existance of files and directories
for i in `seq 0 3`; do
	if [ ! -f ${files[$i]} ]; then
		echo "${files[$i]} does not exist. please ensure it exists in the same path as this script and try again."
		exit $i
	fi
done 

# checking for .htaccess file
if [ ! -f ".htaccess" ] 
	then
	touch ".htaccess"
	cat ".htaccess" <<EOF
<IfModule mod_rewrite.c>
	RewriteEngine on
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^ index.php [L]
</IfModule>
EOF
fi

# reading important variables

# project name
echo "please enter project name: "
read project_name

# capturing new user info for new Mysql db.
echo "the script will create new user to manage your website database. please, supply the following."

echo "supply new user's username: "
read user_name

echo "supply new user's password: "
read user_pass


#==============================================================================#

#creating the project

SRCPATH="$(pwd)"

cd /var/www/html
mkdir "$project_name" "$project_name/img" "$project_name/log"
chmod 755 $project_name -R
chown www-data:www-data $project_name -R

cp -- "$SRCPATH/index.php" "$SRCPATH/sync.sh" "$SRCPATH/headerDesign.php" "$SRCPATH/footerDesign.php" "$SRCPATH/.htaccess" /var/www/html/"$project_name"
if [ $? -ne 0 ]; 
	then 
	echo "error executing the script. please restart."
	unroll
	exit 9 
fi 

cp -R -- "$SRCPATH/includes" "$SRCPATH/classes" "$SRCPATH/js" "$SRCPATH/css" "$SRCPATH/fonts" "$SRCPATH/admin" /var/www/html/"$project_name"
if [ $? -ne 0 ]; 
	then 
	echo "error executing the script. please restart."
	unroll
	exit 10 
fi
echo "done creating project herierchy."

# creating required database and tables for the project. (MySql)
echo "creating database and tables..."

count=0
createDB
while [ $? -ne 0 -a $count -lt 5 ]
do
	createDB
	((count=count+1))
done

if [ $? -ne 0 ] || [ $count -eq 5 ] ; then
	echo "error: script exiting."
	unroll
	exit 1
fi

echo "script finished successfully."
echo "note: to backup your website run the sync.sh script."
exit 0