// TODO: Implement generic AJAX serving requests function.

/* 
 * sendAJAX: this function is used to send AJAX requests (any request).
 * function ips: 	rType 		-> string, method of sending (ex: GET, POST)
 * 				 	rLoc    	-> string, url of the server page to send to
 *					rParams 	-> Assoc. array (optional), contains paramters and values to be embedded in the url for get or post params
 *					rSync 	 	-> request Sync(optional)
 *					rHeaders    -> Assoc. array (optional), contains header keys and values to be set before sending the request
 * function ops:    returns the response recieved from the server
 *
 */
function sendAJAX(rType, rLoc, rParams = null, rSync = false, rHeaders = null) {
	var xhttp;
	if (window.XMLHttpRequest) {
	    xhttp = new XMLHttpRequest();
	} else {
	    // code for IE6, IE5
	    xhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	url = rloc;
	pParams = null;
	if(rType == "GET") {
		// GET REQUEST
		if (rParams != null) {
			url += '?'
			var pcount = rParams.length;
			for (var i in rParams) {
				url += i + "=" + rParams[i];
				pcount--;
				if(pcount <= 1)
					url += "&";
			}
		}	
	} else {
		// POST REQUEST
		if (rParams != null) {
			pParams = "";
			var pcount = rParams.length;
			for (var i in rParams) {
				pParams += i + "=" + rParams[i];
				pcount--;
				if(pcount <= 1)
					pParams += "&";
			}
		}	
	}

	xhttp.open(method, url, rSync);
	if(rHeaders != null) {
		for (var x in rHeaders) {
			xhttp.setRequestHeader(x, rHeaders[x]);
		}
	}
	xhttp.send(pParams);
	return xhttp.responseText;
}