<?php
	/*
		keep it simple.
		this page is the most tiresome page i would have to implement. it requires a precise architecture to do it's supposed functionality. 
		this page should allow adding generic features to the website. by features i mean stuff like: comments, tags, categories, faqs, and so on (those are the basics of the first release.).

		* there are two approachs here. first, limit features to these mentioned above and release features on regular basis. second, promote user to enter his own features allowing him to set the title of the feature, number of inputs, number of outputs, should this feature be joined with another table in the database. let's take for example the tags or the comments feature. the user would enter something like this.
		title : comments
		no of ips: 1 (other users writting comments)
		no of ops: 0 (no specific outputs decided by admin.)
		join this feature to pages table : true.
		
		however the second approach will require lot's of work to give it specific flexiablity and would transform this framework to a framework for lazy to code users (which is not my target. my main target was to automate project creation and database creation not destroy coding skills entirely or make it easy for non coders to code. however, this objective will change in future releases.).

		so to decide, i will go with a hybrid model, i will allow user to enter his features limiting his flexability and give him the option to take built in ready to be integrated features.

		ft->features.
	*/
		require_once("../includes/initialize.php");
		


?>


<?php require_once("adminTopBar.php"); ?>
<div class="container">
	<div class="topbarftch" style="background-color:red;">
		<button class="btn-primary" title="prpdft" onclick="preparedFt()">pre-prepared</button>
		<button class="btn-primary" title="usrft" onclick="userFt()">user-prepared</button>
	</div>
	<div class="ftchoices" style="background-color:black;">
	</div>





<script type="text/javascript">
	window.onload = function() {
		preparedFt();
	}

	function preparedFt()
	{
		var divft = document.getElementByClass("ftchoices");
		divfit.innerHTML = "
		

		"
	}

	function userFt()
	{

	}

</script>
</div>
<?php require_once("adminBottomBar.php"); ?>