<?php
// page UI Done.

/*
	keep it simple.
	
	should have a layout like this.
	--------------------------------------------
	| main admin info editing	     			|
	---------------------------------------------
	| func1		|		all data for			|
	| func2		|		selected				|
	| func3		|		function				|
	---------------------------------------------	
*/

require_once("../includes/initialize.php");

/* 
	sticking to the admin view in the above layout tasks are:
	- get features from admin_options table
	- check for each feature's function existance in the functions.php file 
	- call the function to get what exactly to output.
	- when a func is pressed there should be an AJAX request that fires to the server to update the right bar view.
*/


	// TODO: implement admin checks here. (is he still logged in or something happened on the way)


	// TODO: get all features admin can edit and store them in features array
	$features = ['test1', 'test2', 'test3', 'test4'];

	// TODO: check if these features has table that exists that carries their values. if yes
	//		 retrieve them back. else report an error to admin.
	$values = ['you get to edit this value.', 'you get to edit this value.', 'you get to edit this value.', 'you get to edit this value.'];
?>

<?php require_once("adminTopBar.php");?>
<div class="container">
	<!-- admin functionalities -->
	<div class="leftSideBar">
		<ul>
		<?php
			$featuresSize = count($features);
			for ($i = 0;$i < $featuresSize;$i++)
			{
				if($i % 2 == 0)
					echo "<li class=\"featureItem0\"><strong><a href=\"#\">{$features[$i]}</a></strong></li>";
				else
					echo "<li class=\"featureItem1\"><strong>{$features[$i]}</strong></li>";
			}
		?>
		</ul>
	</div>

	<div class="viewArea">
		<ul>
			<?php
				$featureSelected = true;
				if($featureSelected == true)
				{
					$valuesSize = count($values);
					for ($i = 0;$i < $valuesSize;$i++)
					{
						echo "<li class=\"displayd\">
						<div class=\"displaydup\">
							{$values[$i]}
						</div>
						<div class=\"displayddown\">
							<ul>
								<li><a href=\"editItem.php\">edit</a></li>
								<li><a href=\"editItem.php\">edit</a></li>
								<li><a href=\"editItem.php\">edit</a></li>
							</ul>
						</div>
						</li>";
					}
				}

			?>
		</ul>
	</div>
</div>
<?php require_once("adminBottomBar.php") ?>