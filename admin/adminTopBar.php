<?php 
	//process which page we are currently at 
	$uri = $_SERVER['REQUEST_URI'];
	$lend = strlen($uri);
	$lstrt = 0;
	for ($i = $lend - 1;$uri[$i] != '/' && $i >= 0;$i--)
	{
		$lstrt++;
	}
	$curr_page = substr($uri, $lend - $lstrt, $lstrt);
	

?>

<html>
<head>
	<title>MyTuts Admin</title>
	<link rel="stylesheet" type="text/css" href="css/admin_style.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>

<body>
<header>
	<nav class="navbar navbar-default" style="margin: 0;">
	  <div class="container-fluid">
	    <div class="navbar-header">
	    	<a class="navbar-brand" href="admin_login.php">AdminPanel</a>
	    </div>
	    <ul class="nav navbar-nav">
	    	<li <?php if($curr_page == "admin_view.php") echo "class=\"active\"" ?>><a href="admin_view.php"> admin view </a></li>
	    	<li <?php if($curr_page == "admin_edit.php") echo "class=\"active\"" ?>><a href="admin_edit.php"> edit info </a></li>
	    	<li <?php if($curr_page == "add_feature.php") echo "class=\"active\"" ?>><a href="add_feature.php"> add feature </a></li>
	    	<li <?php if($curr_page == "admin_logout.php") echo "class=\"active\"" ?>><a href="admin_logout.php"> logout </a></li>

	    </ul>
	  </div>
	</nav>
</header>