<?php

/*
	"keep it simple"

	main php script for admin page used to manage the website.
	there should be the following functionalities:
		- create other pages
		- edit other pages
		- delete other pages
		- list all pages with options
		- more...

	there should be a side list bar that implements these functionalities.
*/


require_once("../includes/initialize.php");


// check for data submission for login
if(isset($_POST['submit']))
{
	
	redirect_to("admin_view.php");
}
?>
<html>
<head>
	<title> mytuts admin login </title>
	<link rel="stylesheet" type="text/css" href="css/admin_style.css">
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
</head>
<body>
<form method="POST" class="adloginform" action="admin_login.php">
	<h1>Admin Login</h1>
	<div class="form-group">
		<input type="text" name="adusername" placeholder="admin username" required/>
	</div>
	<div class="form-group">
		<input type="password" name="adpasswd" placeholder="admin password" required/>
	</div>

	<input class="btn btn-primary" type="submit" name="submit" value="submit">

</form>
</body>

</html>