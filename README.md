###							    *************************************************************************
###								| 						Welcome to phpCLI FrameWork generator 			|
###								| the purpose of this framework is to ease the use of reusable code	|
###								| without having to re-copy & paste each time a project is started.		|
###								| this file explains how to use bash scripts to generate the project and|
###								| sync it to mass storage.												|
###								| this framework is intended for programmers to use and make website 	|
###								| programming easier.													|
###								*************************************************************************


FILES DESCRIPTION *all files are .php unless specified*
__________________

this project contains the following files/folders :

- d classes:
	-f account class
	-f admin class
	-f database class
	-f faq class
	-f pages content class
	-f session class
	-f table class

- d includes:
	-f config
	-f functions 
	-f initialize
	-f session
	-f validate functions

- d css:
	-f bootstrap.css
	-f mainCSS.css

- d js:
	-f mainCSS.js
	-f bootstrap.js
	-f jquery-2.2.0.js

- d fonts:
	-f all font files

- f index
- f headerDesign
- f footerDesign

- f start_project.sh
- f sync.sh

###

___________
HOW TO USE


-run the start_project.sh. *note: remember to chmod the script to executable*
-if promoted please provide your sudo password *you may revise the script to make sure it does no harm to ur pc*
-the script will promote you to enter you project name, then it will create the project structure for you in the default path /var/www/html. note that this is the default path because this framework deals with apache2 with php as it's back-end programming language.
-to sync your data (store them somewhere else on mass storage) run the sync.sh script. the script will copy the whole contents of the project to a specified location.