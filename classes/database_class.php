<?php

require_once(INC.DS."config.php");

class MySQLDatabas
{
	
	private $connection;

	function __construct()
	{
		$this->open_connection();		
	}

	private function open_connection()
	{
		$this->connection = mysqli_connect(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

		  // Test if connection succeeded
		if(mysqli_connect_errno()) 
		{
		    die("Database connection failed: " . 
		        mysqli_connect_error() . 
		        " (" . mysqli_connect_errno() . ")"
		    );
		}
	}
	
	public function escape_value($string)
	{					
		$escaped_string = mysqli_real_escape_string($this->connection, $string);		
		return $escaped_string;
	}

	private function confirm_query($result_set)
	{
		if (!$result_set) {
			die("Database query failed.");
		}
	}

	public function fetch_array($result)
	{
		return mysqli_fetch_array($result);
	}

	public function fetch_assoc_array($result)
	{		
		return mysqli_fetch_assoc($result);

	}

	public function num_rows($result)
	{
		return mysqli_num_rows($result);
	}

	public function insert_id()
	{
		return mysqli_insert_id($this->connection);
	}

	public function affected_rows()
	{
		return mysqli_affected_rows($this->connection);
	}

	public function get_connection()
	{
		return $this->connection;
	}

	public function close_connection()
	{
		if (isset($this->connection))
		{
			mysql_close($this->connection);
			unset($this->connection);
		}
	}

	public function query($query)
	{
		$result=mysqli_query($this->connection,$query);	
		$this->confirm_query($result);
		return $result;
	}

	public function query_result_to_array($result)
	{
		$output = array();
		while($row = mysqli_fetch_assoc($result))
		{
			$output[] = $row;
		}
		return $output;
	}
}

$db=new MySQLDatabas();

?>