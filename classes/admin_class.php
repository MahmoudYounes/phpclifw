<?php
require_once("database_class.php");
require_once("table_class.php");


class admin extends table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name="admins";
	protected static $db_fields=['adid', 'username', 'password', 'email', 'clearance_level'];
	//=================================================\\

	public $id="adid";
	public $adid;
	public $username;
	public $email;
	public $password;
	public $clearance_level;

	public static function authenticate($username="",$password="")
	{
		global $db;

		$safe_username=$db->escape_value($username);
		$safe_password=$db->escape_value($password);			

		$query  = "SELECT * ";
		$query .= "FROM ".self::$table_name." ";
		$query .= "WHERE username='{$safe_username}' ";		
		$query .= "LIMIT 1";		
		
		$admin=self::find_by_sql($query);		
		
		if($admin && password_check($password,$admin->password))
		{
			$	
			return $admin;
		}
		
		return false;
	}

	public static function build_on_clearence_level($level)
	{
		#this part should be tweaked to the level of functionallity
		if(!isset($level))
		{
			return "something is wrong";
		}
		switch ($level) {
			// basic functionalities implemented in the FW.
			case 0:
				{
					$output=
					"
					<ul>
						<li>
							<a href=\"edit_options.php\">edit options</a>
						</li>
						<li>
							<a href=\"edit_pages.php\">edit pages</a>
						</li>
						<li>
							<a href=\"edit_account.php\">edit accounts</a>
						</li>
						<li>
							<a href=\"manage_admins.php\">manage admins</a>
						</li>
					</ul>
					";
					break;
				}
			case 1:
				{
					// add your functionalities
					break;
				}	
			case 2:
				{
					// add your functionalities
					break;
				}
			case 3:
				{
					// add your functionalities
					break;
				}
			#case n:
			default:
				{
					$output="you are authorized to do nothing :P .";
					break;
				}
		}
		return $output;
	}

	public function format_admin_for_output_in_table()
	{
		$output=
		"
		<td>{$this->username}</td>
		<td>{$this->email}</td>
		<td>{$this->privilege_level}</td>
		<td><a href=\"actions/remove_admin.php?id={$this->ADID}\">remove</a>
		";
		return $output;

	}

	public static function check_if_admin($id)
	{				
		if(self::find_record_by_id($id))
			return true;
		return false;
	}

	public static function retrieve_all_admins()
	{	
		return self::find_all_records();
	}
	protected function set_id($id)
	{
		$this->ADID=$id;
	}
}