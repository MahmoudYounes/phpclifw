<?php

# this class should provide easy interface to deal with pages stored in the database.

require_once("database_class.php");
require_once("table_class.php");

class pages extends Table
{
	/* private vars */

	//=================================================\\
	
	/* protected vars */
	
	protected static $table_name = "pages";
	protected static $db_fields = ['pid', 'page_title', 'page_content', 'clearance_level', 'attr'];
	//=================================================\\

	public $id = "pid";
	public $pid;
	public $page_title;
	public $page_content;
	public $clearance_level;
	public $attr;

	public static function get_content_by_title($title)
	{
		global $db;
		$safe_title = $db->escape_value($title);		

		$query  = "SELECT * ";
		$query .= "FROM ".self::$table_name." ";
		$query .= "WHERE page_title = '".$safe_title."' ";
			
		return self::find_by_sql($query);	
	}
	
}



?>