<?php
require_once(CLS.DS."database_class.php");

class Table
{
	protected static $table_name;
	protected static $db_fields;
	public static function find_all_records()
	{		
		$query  = "SELECT * ";
		$query .= "FROM ".static::$table_name." ";
	
		return static::find_by_sql($query);						

	}

	public static function find_record_by_id($id)
	{	
		global $db;

		$id_name=static::$db_fields[0];
		$safe_id=$db->escape_value($id);
		
		$query  = "SELECT * ";
		$query .= "FROM ".static::$table_name." ";
		$query .= "WHERE {$id_name}={$safe_id} ";
		
		return static::find_by_sql($query);

	}

	public static function find_by_sql($sql="")
	{
		global $db;	
	
		$result= $db->query($sql);	
		$rows_affected=$db->num_rows($result);	
		if ($rows_affected>1)										    				//result set has more than one row						
			return static::build_more_of_me($db->query_result_to_array($result));		//this function returns array of associative arrays including all result set									
		else if($rows_affected==1)																							
			return static::build_me($db->fetch_assoc_array($result));					// returns one associative array for the poor one row returned												
			
	}


	/*
		-this function is responsible for converting an array valid to be the calling class object to a calling class object
		-Input: array valid to be an account object (validity comes from private: no outside class can call this method)
		-Ouput: an acount object
	*/
	private static function build_me($arr)
	{		
		$class_name=get_called_class();			
		$object=new $class_name;			
		foreach ($arr as $attribute=>$value)
		{
			if($object->has_attribute($attribute))
			{
				$object->$attribute=$value;
			}
		}		
		return $object;	
	}

	private static function build_more_of_me($arr)
	{
		$class_name=get_called_class();
		$objects=array();
		foreach($arr as $elem)		
			$objects[]=self::build_me($elem);

		return $objects;
	
	}

	/*
		-this function checks if the class has the input attribute or no
		-Input: attribute required to be checked
		-Output: true if the class contains the required Attribtue, false otherwise
	*/
	private function has_attribute($attribute)
	{
		$object_vars=$this->attributes();
		return array_key_exists($attribute, $object_vars);	

	}

	/*
		-this function returns assoc array of key & value of the attributes of the class
		-notes: this function is inherited and resolves the class calling it 
		-Input: nothing at all
		-Ouput: assoc array of keys and values
	*/
	protected function attributes()
	{
		$attributes=array();		
		foreach(static::$db_fields as $field) 
		{
			if (property_exists($this, $field))
			{				
				$attributes[$field]=$this->$field;
			}
		}
		return $attributes;
	}

	protected function clean_attributes()
	{
		global $db;

		$clean_arr=array();
		foreach ($this->attributes() as $key => $value)		
			$clean_arr[$key]=$db->escape_value($value);

		return $clean_arr;
		
	}

	/*
		-this function is responsible for updating or creating the object
		-Input: none
		-output: true or false
	*/
	public function save()	
	{				
		return isset(static::$id)? static::update():static::create();
	}

	public function create()
	{			
		global $db;

		$inputs=$this->clean_attributes();				
		$query  = "INSERT INTO ".static::$table_name." (";
		$query .= join(", ",array_keys($inputs));
		$query .=") VALUES ('";
		$query .= join("', '",array_values($inputs));		
		$query.="')";

		if($db->query($query))
		{
			$temp_id=$db->insert_id();
			$this->set_id($temp_id);			
			return self::find_record_by_id($temp_id);
		}		
		return false;
	}

	public function update()
	{
		global $db;
				
		$inputs = $this->clean_attributes();
		$modified_inputs=array();
		foreach ($inputs as $key=>$value)		
			$modified_inputs[]="{$key}='{$value}'";
		
		$id_name=$this->id;			
		$query  = "UPDATE ".static::$table_name." SET ";
		$query .= join(", ",$modified_inputs);
		$query .= " WHERE ".$id_name."=".$db->escape_value($this->$id_name);				//this move is made in order for different classes to have different names for their id without confusion
		
		return $db->query($query);
	}

	public function delete()
	{
		global $db;

		$id_name= $this->id;
		$query  = "DELETE FROM ".static::$table_name;
		$query .= " WHERE ".$id_name."=".$db->escape_value($this->$id_name);
				
		return $db->query($query);

	}

	/*
		-this function is responsible for populating a created object with a given array
		-Input: array that has same name of fields as fields of the class
		-Output: nothing
	*/
	public function populate_object($arr)
	{
		$attributes=array();
		foreach(self::$db_fields as $field) 
		{
			if (property_exists($this, $field)&&array_key_exists($field, $arr))
			{
				$attributes[$field]=$arr[$field];
			}
			else
				die("error happened at class account");
		}
	}

	protected function set_id($id){}
}

?>