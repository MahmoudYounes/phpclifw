<?php

class Session
{
	private $logged_in;			
	private $message;


	public $user_id;

	function __construct()	
	{
		session_start();
		$this->check_login();
	}

	public function is_logged_in() 							// is the user logged in ??
	{		
		return $this->logged_in;
	}

	public function mark_login($user)						// if the user is logged in please php populate the session and this class with data
	{			
		if ($user)
		{					
			$this->user_id = $_SESSION['id']=$user->AID;
			$this->logged_in = true;			
		}
	}

	public function mark_admin_login($user)						// if the user is logged in please php populate the session and this class with data
	{			
		if ($user)
		{					
			$this->user_id = $_SESSION['id']=$user->ADID;
			$this->logged_in = true;			
		}
	}
	
	public function mark_logout()							// if someone is logging out please php take actions
	{
		unset($_SESSION['id']);
		unset($this->user_id);
		$this->logged_in=false;

	}

	public function set_message($message="")
	{
		$_SESSION['message']=$message;
	}

	public function get_message()
	{
		if (isset($_SESSION['message']))
		{
			$message = $_SESSION['message'];
			unset($_SESSION['message']);
			return $message;
		}		
	}

	public function is_admin()
	{				
		if($this->is_logged_in())
		{			
			return admin::check_if_admin(self::get_current_id());
		}
		return false;
	}

	private function check_login() 							//checks if previously logged in ... useful for remember me
	{
		if(isset($_SESSION['id']))
		{			
			$this->user_id=$_SESSION['id'];
			$this->logged_in=true;
		}
		else
		{
			unset($this->user_id);
			$this->logged_in=false;
		}
	}
	public function get_current_id()
	{
		if($this->logged_in)
			return $_SESSION['id'];
		else
			return false;
	}
	
}

if (!isset($session))
	$session = new Session();

?>