#!/bin/bash

#====================================================#
# this shell script syncs data from here to mass     #
# storage unit.                                      #
#====================================================#

echo "Syncing Script Started\n"
echo "notes: this script will create a backup folder for your project."

#if condition to check if this script has root previligies
if [ "$(whoami)" != "root" ];
then
        echo "need to be root to execute this shell script. re-executing..."
        exec sudo -- "$0" "$@"
fi

echo "enter absolute path of the location to sync."

read SyncLocation

if [ ! -d "$SyncLocation" ]; then
    mkdir "$SyncLocation"
fi

# saving project
cp -r . "$SyncLocation"
if [ $? -ne 0 ]; then
    
    echo "syncing failed. please re-run the Script"
    exit 1
fi  

# saving database
# reading user info
infoFile=$(pwd)/includes/.bu.txt
echo $infoFile

count=1
while read -r line || [[ -n "$line" ]]; do
    if [ $count -eq 1 ]; then
        username=$line
        echo $username
    fi
    if [ $count -eq 2 ]; then
        password=$line
        echo $password
    fi
    if [ $count -eq 3 ]; then
        project=$line
        echo $project
    fi

    ((count=count + 1))
done < $infoFile


mkdir $SyncLocation/MySqlDb
sqlBackupFile=${SyncLocation}/MySqlDb/${project}.sql

mysqldump --opt --user=${username} --password=${password} ${project} > ${sqlBackupFile}

if [ $? -ne 0 ]; then
    
    echo "syncing failed. please re-run the Script"
    exit 1
else
    echo "syncing done."
fi  


